
<div id="manageGroupView" class="myForm">
    <button type="button" class="close" aria-label="Close" onclick="hideManageGroupView()">
        <span aria-hidden="true">&times;</span>
    </button>

    <form id="manageUserGroupForm" target="dummyframeManageGroup" autocomplete="off">
        <div>
            Gerer le groupe <strong>{{ groupeActuel.nom }}</strong>
        </div>

        <?php
            if(isset($_SESSION["user"])) {
                echo(
                    '<div class="form-group" v-if="groupeActuel.prive">'.
                        '<label for="groupEmail">Accorder l\'accès a une personne</label>'.
                        '<input type="email" class="form-control" id="groupEmail" placeholder="christophe.dumas@pro.fr" required style="margin-bottom: 10px">'.
                        '<button class="btn btn-success" @click="autoriser(groupeActuel.id)" style="margin-right: 20px">Accorder l\'accès</button>'.
                        '<button class="btn btn-danger" @click="interdire(groupeActuel.id)">Révoquer l\'accès</button>'.
                    '</div>'
                );
            }  
        ?>
    </form>

    <form id="manageGroupForm" target="dummyframeManageGroup" autocomplete="off">
        <?php
            if(isset($_SESSION["user"])) {
                echo(
                    '<div>'.
                        '<button class="btn btn-primary"  @click="visibilite(groupeActuel.id, ! groupeActuel.prive)">Rendre <strong>{{ groupeActuel.nom }}</strong> {{ groupeActuel.prive ? \'public\' : \'privé\' }}</button>'.
                    '</div>'
                );
            }  
        ?>

        <button class="btn btn-danger" @click="supprimer(groupeActuel.id)">Supprimer <strong>{{ groupeActuel.nom }}</strong></button> 
    </form>

    <iframe name="dummyframeManageGroup" id="dummyframeManageGroup" style="display: none;"></iframe>
</div>
