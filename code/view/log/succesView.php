<div class="alert alert-success alert-dismissible fade show" style="z-index: 1000;" role="alert">
  <?php
    echo($success);
  ?>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>