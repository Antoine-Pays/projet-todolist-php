<?php
require_once(__DIR__.'/gateway/userGateway.php');

class mdlUser {
    static function connexion($email, $password){

        if(! validation::val_login($email, $password)) return;
            
        $user = userGateway::getUser($email, $password);
        if($user === NULL) return error("Email ou Mot de passe invalide");
   
        $_SESSION['user'] = $user;
        $_SESSION['role'] = "user";

        return success("Bien vu c'est le bon mdp !");
    }

    static function register($nom, $prenom, $email, $password) {
        if(! validation::val_register($nom, $prenom, $email, $password)) return;
            
        $user = userGateway::newUser($nom, $prenom, $email, $password);
        if ($user === false) return error("Cet email est déja utilisé");
            
        $_SESSION['user'] = $user;
        $_SESSION['role'] = "user";

        return success("Tu as maintenant un compte sur Toudou Liste");
    }

    static function deconnexion() {
        session_start();
        $_SESSION = [];
        session_destroy();
    }

    static function isUser() : bool {
        if(isset($_SESSION['user']) && isset($_SESSION['role'])) {
            return true;
        } else {
            return false;
        }
    }
}