<?php

Class Task implements JsonSerializable{
    private $nom;
    private $isDone;
    private $id;

    function __construct(string $nom, int $id, bool $isDone = false){
        $this->nom=$nom;
        $this->isDone=$isDone;
        $this->id=$id;
    }

    function get_nom(): string {
        return $this->nom;
    }

    function set_nom(string $nom){
        $this->nom = $nom;
    }

    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }
}