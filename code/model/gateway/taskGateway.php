<?php
require_once(__DIR__."/../task.php");
require_once(__DIR__."/../../controller/session.php");
require_once(__DIR__.'/../../controller/connection.php');

class taskGateway {
    static function getTask(String $groupId) {

        $tab = []; 
        $conn = Connection::getConnection();
        
        $userId = isset($_SESSION["user"]) ? $_SESSION["user"]->get_id() : -1;
        
        $query = "SELECT * FROM tasks t WHERE t.group_id = :idgroup";
        $results = $conn->executeQuery(
            $query,
            [
                ':idgroup'=> array($groupId, PDO::PARAM_INT)
            ]
        );
        
        if($results == NULL) return NULL;
        
        foreach($results as $groupResult) {
            $tab[] = new Task($groupResult["name"], $groupResult["id"], $groupResult["done"]);
        }
        
        return $tab;
    }
    
    static function createTask(String $nom, String $groupId){
    
        $conn = Connection::getConnection();

        $query = "INSERT INTO tasks(group_id, name, done) VALUES(:idgroup, :nom, 0)";
        $conn->executeUpdate(
            $query,
            [
                ':idgroup' => array($groupId, PDO::PARAM_INT),
                ':nom'=> array($nom, PDO::PARAM_STR)
            ]
        );
               

        return new Task($nom, $conn->lastInsertId());
    }

    static function deleteTask(String $taskId){
        $conn = Connection::getConnection();

        $query = "DELETE FROM tasks WHERE id=:idTask";
        $conn->executeUpdate(
            $query,
            [
                ':idTask' => array($taskId, PDO::PARAM_INT)
            ]
        );
    }

    static function completeTask(String $taskId, bool $etat) {
        $conn = Connection::getConnection();

        $query = "UPDATE tasks SET done = :etat WHERE id = :idTask";
        $conn->executeUpdate(
            $query,
            [
                ':idTask' => array($taskId, PDO::PARAM_INT),
                ':etat' => array($etat, PDO::PARAM_BOOL)
            ]
        );
    }
}
