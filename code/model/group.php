<?php

Class Group implements JsonSerializable {
    private $nom;
    private $prive;
    private $id;

    function __construct(string $nom, int $id, bool $prive = false){
        $this->nom = $nom;
        $this->prive = $prive;
        $this->id = $id;
    }

    function get_nom(): string {
        return $this->nom;
    }
    
    function is_prive(): bool {
        return $this->prive;
    }

    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }
}