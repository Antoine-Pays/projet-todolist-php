<?php

require_once(__DIR__."/../config/config.php");

class Connection extends PDO
{
    private static $connection;


    public static function getConnection() : Connection
    {
        if(is_null(Connection::$connection))
        {
            Connection::$connection = new Connection(Config::$dsn, Config::$user, Config::$password);   
        }

        return Connection::$connection;
    }


    public function __construct(string $dsn, string $username, string $password)
    {
        parent::__construct($dsn, $username, $password);
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /*** @paramstring $query
     * @paramarray$parameters
     * @return boolReturns`true` on success, `false` otherwise
     */
    public function executeQuery(string $query, array $parameters = []): array
    {
        $stmt = parent::prepare($query);

        foreach ($parameters as $name => $value) 
        {
            $stmt->bindValue($name, $value[0], $value[1]);
        }

        $stmt->execute();

        return $stmt->fetchall();
    }

    public function executeUpdate(string $query, array $parameters = []): bool
    {
        $stmt = parent::prepare($query);

        foreach ($parameters as $name => $value) 
        {
            $stmt->bindValue($name, $value[0], $value[1]);
        }

        return $stmt->execute();
    }
}
