<?php
require_once(__DIR__.'/../../controller/connection.php');
require_once(__DIR__.'/../user.php');

class userGateway{
    static function getUser(string $email, string $password) {
        $conn = Connection::getConnection();
        $query = "SELECT * FROM users WHERE email=:email";
        
        $resutls = $conn->executeQuery(
            $query, 
            [
                ':email'=> array($email, PDO::PARAM_STR)
            ]
        );
    
        if($resutls == NULL) return NULL;
        $userResutls = $resutls[0];
    
        if(! password_verify($password, $userResutls["password"])) {
            return NULL;
        }
    
        return new User(intval($userResutls['id']), $userResutls['nom'], $userResutls['prenom'], $userResutls['email']);
    
    }
    
    static function newUser(string $nom, string $prenom, string $email, string $password) {
        $conn = Connection::getConnection();
        $query = "SELECT * FROM users WHERE email=:email";
        $resutls = $conn->executeQuery($query, array(':email'=> array($email, PDO::PARAM_STR)));
        
        if($resutls != NULL) return false;
        
        $query = "INSERT INTO users(nom, prenom, email, password) VALUES(:nom, :prenom, :email, :password)";
    
        $conn->executeUpdate(
            $query,
            [
                ':nom'=> array($nom, PDO::PARAM_STR), 
                ':prenom'=> array($prenom, PDO::PARAM_STR),
                ':email'=> array($email, PDO::PARAM_STR),
                ':password'=> array(
                    password_hash(
                        $password, 
                        PASSWORD_BCRYPT
                    ),
                    PDO::PARAM_STR
                )
            ]
        );
        
        return self::getUser($email, $password);
    }
}

