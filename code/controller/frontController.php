<?php

require_once(__DIR__ . "/message.php");
require_once(__DIR__ . '/session.php');
require_once(__DIR__ . '/controllerVisiteur.php');
require_once(__DIR__ . '/controllerUser.php');
require_once(__DIR__.'/../model/mdlUser.php');
require_once(__DIR__.'/../model/user.php');

global $action;
$listeAction_User = array('logout', 'group', 'createGroup', 'deleteGroup', 'authorizeUser', 'rejectUser', 'groupVisibility' ,'task', 'createTask', 'deleteTask', 'completeTask');

try {
    if (isset($_REQUEST['action']) && !empty($_REQUEST['action'])) {
        $action = $_REQUEST['action'];
    }

    if (in_array($action, $listeAction_User) && mdlUser::isUser()) {
        $user = new controllerUser();
    } else {
        $visiteur = new controllerVisiteur();
    }
} catch (PDOException $e) {
    error("Erreur inattendue BDD");
} catch (Exception $e2) {
    error("Erreur inattendue");
}
