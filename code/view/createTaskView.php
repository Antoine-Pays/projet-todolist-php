
<div id="createTaskView" class="myForm">
    <button type="button" class="close" aria-label="Close" onclick="hideCreateTaskView()">
        <span aria-hidden="true">&times;</span>
    </button>

    <form id="createTaskForm" target="dummyframeCreateTask" autocomplete="off">
        <div class="form-group">
            <label for="taskName">Nom</label>
            <input type="text" class="form-control" id="taskName" placeholder="Important" required>
        </div>

        <button class="btn btn-primary" onclick="createTask()">Créer</button> 
    </form>

    <iframe name="dummyframeCreateTask" id="dummyframeCreateTask" style="display: none;"></iframe>
</div>
