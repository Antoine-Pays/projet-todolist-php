
<div id="registerView" class="myForm">
    <button type="button" class="close" aria-label="Close" onclick="hideRegisterView()">
        <span aria-hidden="true">&times;</span>
    </button>

    <form id="registerForm" target="dummyframeRegister">
        <div class="form-group">
            <label for="registerPrenom">Prenom</label>
            <input type="text" class="form-control" id="registerPrenom" placeholder="Christophe" required>
        </div>
        <div class="form-group">
            <label for="registerNom">Nom</label>
            <input type="text" class="form-control" id="registerNom" placeholder="Dumas" required>
        </div>
        <div class="form-group">
            <label for="registerEmail">Email</label>
            <input type="email" class="form-control" id="registerEmail" placeholder="christophe.dumas@pro.fr" required>
        </div>
        <div class="form-group">
            <label for="registerMotDePasse">Mot de passe</label>
            <input type="password" class="form-control" id="registerMotDePasse" placeholder="AZt013uXf" required>
        </div>
        <button class="btn btn-primary" onclick="register()">S'inscrire</button>
    </form>

    <iframe name="dummyframeRegister" id="dummyframeRegister" style="display: none;"></iframe>
</div>
