<?php
    require_once(__DIR__."/../../controller/session.php");
?>

<nav id="header" class="navbar navbar-expand-lg navbar-dark bg-dark">
    <img src="public/images/logo.svg" width="30" height="30" class="d-inline-block align-top" alt="">
    
    <a class="navbar-brand" href="#" style="margin-left: 20px">Toudou Liste</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div class="navbar-nav mr-auto">
        
        </div>
        <form class="form-inline my-2 my-lg-0">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <?php
                    if(! isset($_SESSION["user"])) {
                        require_once(__DIR__."/notConnected.php");
                    }  
                    else 
                        require_once(__DIR__."/connected.php");
                    ?>
            </div>
        </form>
    </div>
</nav>