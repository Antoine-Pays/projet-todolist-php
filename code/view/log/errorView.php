<div class="alert alert-danger alert-dismissible fade show" style="z-index: 1000;" role="alert">
  <?php
    echo($error);
  ?>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>