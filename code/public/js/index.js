let inMenu = false;


function log(text) {
     
  let child = document.createElement('div');
  child.innerHTML = text;

  setTimeout(() => { child.remove() }, 3000);
  
  document.getElementById("log").appendChild(child);
}

function register() {
    let form = $("#registerForm")

    let els = form[0].elements

    let result = $.ajax({
        type: 'POST',
        url: 'controller/frontController.php?action=register',
        async: false,
        data:
            (
                (els[0].value != '' ? `&prenom=${els[0].value}` : '') + 
                (els[1].value != '' ? `&nom=${els[1].value}` : '') + 
                (els[2].value != '' ? `&email=${els[2].value}` : '') + 
                (els[3].value != '' ? `&motdepasse=${els[3].value}` : '')
            ).substring(1)
    });

    if(result.status != 200) {
        log(result.responseText);
        return;
    }

    document.location.reload();
}

function login() {
    let form = $("#loginForm")

    let els = form[0].elements

    let result = $.ajax({
        type: 'POST',
        url: 'controller/frontController.php?action=login',
        async: false,
        data:
            (
                (els[0].value != '' ? `&email=${els[0].value}` : '') + 
                (els[1].value != '' ? `&motdepasse=${els[1].value}` : '')
            ).substring(1)
    });

    if(result.status != 200) {
        log(result.responseText);
        return;
    }

    document.location.reload();
}

function createGroup() {
    let form = $("#createGroupForm")

    let els = form[0].elements

    let result = $.ajax({
        type: 'POST',
        url: 'controller/frontController.php?action=createGroup',
        async: false,
        data:
            (
                (els[0].value != '' ? `&nom=${els[0].value}` : '') + 
                (`&privee=${els[1].checked}`)
            ).substring(1)
    });

    if(result.status != 200) {
        log(result.responseText);
        return;
    }

    if(container.$data.groupes == null) container.$data.groupes = []

    container.$data.groupes.push(JSON.parse(result.responseText))
    container.$data.groupeActuel = container.$data.groupes[container.$data.groupes.length - 1]
    
    els[0].value = ''
    els[1].checked = false

    hideCreateGroupView();
}

function createTask() {
    let form = $("#createTaskForm")

    let els = form[0].elements

    let result = $.ajax({
        type: 'POST',
        url: 'controller/frontController.php?action=createTask',
        async: false,
        data:
            (
                (els[0].value != '' ? `&nom=${els[0].value}` : '') +
                (`&group=${container.$data.groupeActuel.id}`)
            ).substring(1)
    });

    
    if(result.status != 200) {
        log(result.responseText);
        return;
    }

    if(container.$data.taches == null) container.$data.taches = []

    container.$data.taches.push(JSON.parse(result.responseText))

    els[0].value = ''

    hideCreateTaskView()
}

function logout() {
    $.ajax({
        url: 'controller/frontController.php?action=logout',
        type: 'GET',
        async: false
    });

    location.reload()
}

function hideRegisterView() {
    document.getElementById('registerView').style.display = 'none';
    document.getElementById('form-connection').style.display = 'none'

    inMenu = false;
}

function hideLoginView() {
    document.getElementById('loginView').style.display = 'none';
    document.getElementById('form-connection').style.display = 'none'

    inMenu = false;
}

function hideCreateGroupView() {
    document.getElementById('createGroupView').style.display = 'none';
    document.getElementById('form-connection').style.display = 'none'

    inMenu = false;
}

function hideCreateTaskView() {
    document.getElementById('createTaskView').style.display = 'none';
    document.getElementById('form-connection').style.display = 'none'

    inMenu = false;
}

function hideManageGroupView() {
    document.getElementById('manageGroupView').style.display = 'none';
    document.getElementById('form-connection').style.display = 'none'

    inMenu = false;
}

function showRegisterView() {
    document.getElementById('registerView').style.display = 'block';
    document.getElementById('form-connection').style.display = 'block'

    inMenu = true;
}

function showLoginView() {
    document.getElementById('loginView').style.display = 'block';
    document.getElementById('form-connection').style.display = 'block'

    inMenu = true;
}

function showCreateGroupView() {
    document.getElementById('createGroupView').style.display = 'block';
    document.getElementById('form-connection').style.display = 'block'

    inMenu = true;
}

function showCreateTaskView() {
    document.getElementById('createTaskView').style.display = 'block';
    document.getElementById('form-connection').style.display = 'block'

    inMenu = true;
}

function showManageGroupView() {
    document.getElementById('manageGroupView').style.display = 'block';
    document.getElementById('form-connection').style.display = 'block'

    inMenu = true;
}