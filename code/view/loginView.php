
<div id="loginView" class="myForm">
    <button type="button" class="close" aria-label="Close" onclick="hideLoginView()">
        <span aria-hidden="true">&times;</span>
    </button>

    <form id="loginForm" target="dummyframeLogin">
        <div class="form-group">
            <label for="loginEmail">Email</label>
            <input type="email" class="form-control" id="loginEmail" placeholder="christophe.dumas@pro.fr" required>
        </div>
        <div class="form-group">
            <label for="loginMotDePasse">Mot de passe</label>
            <input type="password" class="form-control" id="loginMotDePasse" placeholder="AZt013uXf" required>  
        </div>
        <button class="btn btn-primary" onclick="login()">Se connecter</button> 
    </form>

    <iframe name="dummyframeLogin" id="dummyframeLogin" style="display: none;"></iframe>
</div>
