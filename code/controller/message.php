<?php

    function error(string $error = NULL) : bool
    {
        if($error == NULL) $error = "Une erreur est survenue";

        require_once(__DIR__."/../view/log/errorView.php");

        http_response_code(400);
        return false;
    }

    function success(string $success = NULL) : bool
    {
        if($success == NULL) $success = "Réussite";

        require_once(__DIR__."/../view/log/succesView.php");

        http_response_code(200);
        return true;
    }