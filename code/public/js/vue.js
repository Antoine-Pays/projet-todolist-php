function loadGroup() {
  let result = $.ajax({
    type: 'POST',
    url: 'controller/frontController.php?action=group',
    async: false
  });

  if(result.status != 200) {
    log(result.responseText);
    return;
  }

  return JSON.parse(result.responseText)
}

function loadTaches(group) {
  let result = $.ajax({
    type: 'POST',
    url: 'controller/frontController.php?action=task',
    async: false,
    data:
        (
          `&group=${group}`
        ).substring(1)
  });

  if(result.status != 200) {
    log(result.responseText);
    return;
  }

  return JSON.parse(result.responseText)
}

function supprimerUneTache(tacheId) {
  let result = $.ajax({
    type: 'POST',
    url: 'controller/frontController.php?action=deleteTask',
    async: false,
    data:
        (
          `&group=${container.$data.groupeActuel.id}` +
          `&taskId=${tacheId}`
        ).substring(1)
  });

  if(result.status != 200) {
    log(result.responseText);
    return false;
  }

  return true
}

function completerUneTache(tacheId) {
  let result = $.ajax({
    type: 'POST',
    url: 'controller/frontController.php?action=completeTask',
    async: false,
    data:
        (
          `&group=${container.$data.groupeActuel.id}` +
          `&taskId=${tacheId}` +
          `&completed=${! container.$data.taches.find(tache => tache.id == tacheId).isDone}`
        ).substring(1)
  });

  if(result.status != 200) {
    log(result.responseText);
    return false;
  }

  return true
}

function autoriserUnUtilisateur(groupeId) {
  let form = $("#manageUserGroupForm")

  let els = form[0].elements

  let result = $.ajax({
      type: 'POST',
      url: 'controller/frontController.php?action=authorizeUser',
      async: false,
      data:
          (
              (els[0].value != '' ? `&email=${els[0].value}` : '') + 
              (`&group=${groupeId}`)
          ).substring(1)
  });

  log(result.responseText);
  
  if(result.status == 200) {
    hideManageGroupView();
    els[0].value = ''
  }
}

function interdireUnUtilisateur(groupeId) {
  let form = $("#manageUserGroupForm")

  let els = form[0].elements

  let result = $.ajax({
      type: 'POST',
      url: 'controller/frontController.php?action=rejectUser',
      async: false,
      data:
          (
              (els[0].value != '' ? `&email=${els[0].value}` : '') + 
              (`&group=${groupeId}`)
          ).substring(1)
  });

 
  log(result.responseText);
  
  if(result.status == 200) {
    hideManageGroupView();
    els[0].value = ''
  }
}

function changerVisibilite(groupeId, prive) {
  let result = $.ajax({
      type: 'POST',
      url: 'controller/frontController.php?action=groupVisibility',
      async: false,
      data:
          (
              (`&group=${groupeId}`) + 
              (`&visibility=${prive}`)
          ).substring(1)
  });

  log(result.responseText);
  
  if(result.status == 200) {
    hideManageGroupView();
    container.$data.groupeActuel.prive = ! container.$data.groupeActuel.prive
  }
}

function supprimerGroupe(groupeId) {
  let result = $.ajax({
      type: 'POST',
      url: 'controller/frontController.php?action=deleteGroup',
      async: false,
      data:
          (
              (`&group=${groupeId}`)
          ).substring(1)
  });

  log(result.responseText);
  
  if(result.status == 200) {
    hideManageGroupView();
    return true;
  }
  
  return false
}

let container = new Vue({
  el: '#container',
  data: {
    groupes: [],
    taches: [],
    groupeActuel: Object
  },
  mounted: function() {
      this.groupes = loadGroup()
      if(!this.groupes) return;

      this.groupeActuel = this.groupes[0]

      this.taches = loadTaches(this.groupeActuel.id)

      setInterval(() => {
        this.groupes = loadGroup()
        
        if(this.groupes.find(groupe => groupe.id == this.groupeActuel.id) == undefined) {
          this.groupeActuel = this.groupes[0]
        }
        else {
          this.groupeActuel = this.groupes.find(groupe => groupe.id == this.groupeActuel.id)
        }

        this.taches = loadTaches(this.groupeActuel.id)
      }, 5000)
  },
  methods: {
    completerTache: function(tacheId) {
      if(completerUneTache(tacheId)) {
        let tache = this.taches.find(tache => tache.id == tacheId)
        tache.isDone = ! tache.isDone
      }
    },
    changerGroupe: function(groupeId) {
      this.taches = loadTaches(groupeId)

      this.groupeActuel = this.groupes.find(groupe => groupe.id == groupeId)
    },
    supprimerTache: function(tacheId) {
      if(supprimerUneTache(tacheId)) {
        this.taches = this.taches.filter(tache => tache.id != tacheId);
      }
    },
    gererGroupe: function(groupeId) {
      showManageGroupView(groupeId);
    },
    autoriser: function(groupeId) {
      autoriserUnUtilisateur(groupeId);
    },
    interdire: function(groupeId) {
      interdireUnUtilisateur(groupeId);
    },
    visibilite: function(groupeId, prive) {
      changerVisibilite(groupeId, prive);
    },
    supprimer: function(groupeId) {
      if(supprimerGroupe(groupeId)) {
        this.groupes = this.groupes.filter(groupe => groupe.id != groupeId);
        this.groupeActuel = this.groupes.length > 1 ? this.groupes[0] : {}
      }
    }
  }
})