<html>
    <?php 
        require_once(__DIR__."/config/config.php");
        
        require_once(__DIR__.'/config/autoload.php');
        Autoload::charger();
    ?>

    <head>
        <title>ToudouListe</title>
        <!-- CSS -->    
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="public/css/style.css">
 
    </head>
   
    <body>
        <?php
            require_once(__DIR__."/view/header/header.php");
        ?>

        <div id="log"></div>

        <div id="container">
            <?php
                require_once(__DIR__."/view/container/menu.php");
                require_once(__DIR__."/view/container/content.php");
            ?>

            <div id="form-connection">
                <?php
                    require_once(__DIR__."/view/registerView.php");
                    require_once(__DIR__."/view/loginView.php");
                    require_once(__DIR__."/view/createGroupView.php");
                    require_once(__DIR__."/view/createTaskView.php");
                    require_once(__DIR__."/view/manageGroupView.php");
                ?>
            </div>
        </div>

        <!-- jQuery and JS bundle w/ Popper.js -->
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="public/js/index.js"></script>
        <script src="public/js/vue.js"></script>
    </body>
</html>