
<div id="createGroupView" class="myForm">
    <button type="button" class="close" aria-label="Close" onclick="hideCreateGroupView()">
        <span aria-hidden="true">&times;</span>
    </button>

    <form id="createGroupForm" target="dummyframeCreateGroup" autocomplete="off">
        <div class="form-group">
            <label for="groupName">Nom</label>
            <input type="text" class="form-control" id="groupName" placeholder="Important" required>
        </div>
        <?php
            if(isset($_SESSION["user"])) {
                echo(
                '<div class="form-check">'.
                    '<input type="checkbox" class="form-check-input" id="groupPrivate">'.
                    '<label class="form-check-label" for="groupPrivate">Privée</label>'.
                '</div>'
            );
            }  
        ?>

        <button class="btn btn-primary" onclick="createGroup()">Créer</button> 
    </form>

    <iframe name="dummyframeCreateGroup" id="dummyframeCreateGroup" style="display: none;"></iframe>
</div>
