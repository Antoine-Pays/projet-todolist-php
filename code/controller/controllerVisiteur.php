<?php
require_once(__DIR__."/message.php");
require_once(__DIR__.'/../model/mdlUser.php');
require_once(__DIR__.'/../model/mdlGroup.php');
require_once(__DIR__.'/../model/mdlTask.php');

class controllerVisiteur{
    function __construct()
    {
        global $action;

        try {
            switch ($action) {
                case 'NULL':
                    
                break;
                
                case "login":
                    $email = $_POST["email"];
                    $password = $_POST["motdepasse"];
                    mdlUser::connexion($email, $password);

                    break;    
        
                case "register":
                    $nom = $_POST["nom"];
                    $prenom = $_POST["prenom"];
                    $email = $_POST["email"];
                    $password = $_POST["motdepasse"];
                    mdlUser::register($nom, $prenom, $email, $password);

                    break;

                case 'group':
                    mdlGroup::getGroup();

                    break;

                case 'createGroup':
                    $nom = $_POST["nom"];

                    mdlGroup::createGroup($nom);

                    break;

                case 'deleteGroup':
                    $groupId = $_POST["group"];
    
                    mdlGroup::deleteGroup($groupId);
    
                    break;
                
                case 'task':
                    $groupId = $_POST["group"];
                    mdlTask::getTask($groupId);
                    
                    break;

                case 'createTask':
                    $nom = $_POST["nom"];
                    $group = $_POST["group"];
                        
                    mdlTask::createTask($nom, $group);

                    break;

                case 'deleteTask':
                    $groupId = $_POST["group"];
                    $taskId = $_POST["taskId"];
                    mdlTask::deleteTask($taskId, $groupId);
            
                    break;

                case 'completeTask':
                    $groupId = $_POST["group"];
                    $taskId = $_POST["taskId"];
                    $completed = $_POST["completed"];
                    mdlTask::completeTask($groupId, $taskId, $completed);
            
                    break;        
                        
                default:
                    error("Erreur d'appel controlleur visiteur");
                    break;
            }
                
        } catch (PDOException $e) {
            error("Erreur inattendue BDD controller visiteur". $e);
        } catch (Exception $e2) {
            error("Erreur inattendue controller visiteur". $e2);
        }
    }
    
}
