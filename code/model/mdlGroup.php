<?php
require_once(__DIR__.'/gateway/groupGateway.php');
require_once(__DIR__.'/../controller/connection.php');
require_once(__DIR__.'/../controller/message.php');
require_once(__DIR__.'/../controller/session.php');

class mdlGroup {

    static function getGroup(){
        
        $results = groupGateway::getGroup();
        echo json_encode($results);
    }

    static function createGroup(String $nom, String $privee = ''){
        if(! validation::val_createGroup($nom, $privee)) return;
       
        if(! isset($privee) || $privee == "") {
            $privee = false;
        }
        else {
            $privee = filter_var($privee, FILTER_VALIDATE_BOOLEAN);
        }

        $group = groupGateway::createGroup($nom, $privee);

        echo json_encode($group);
    }

    static function deleteGroup(String $groupId){

        if(groupGateway::isPrivate($groupId)) {
            if(!isset($_SESSION["user"])) 
                return error("Vous n'etes pas connecté");
            
            if(!groupGateway::hasAccess($groupId, $_SESSION["user"]->get_id())) 
                return error("Vous n'avez pas la permission de supprimer ce groupe");
        }

        groupGateway::deleteGroup($groupId);

    }

    static function authorize(String $email, String $groupId) {
        if(! validation::val_email($email)) return;

        if(groupGateway::isPrivate($groupId)) {
            if(!isset($_SESSION["user"])) 
                return error("Vous n'etes pas connecté");
            
            if(!groupGateway::hasAccess($groupId, $_SESSION["user"]->get_id())) 
                return error("Vous n'avez pas la permission d'autoriser");
        }

        if(! groupGateway::authorize($email, $groupId)) return;

        return success("Ajout autorisation réussie");
    }

    static function revoke(String $email, String $groupId) {
        if(! validation::val_email($email)) return;


        if(groupGateway::isPrivate($groupId)) {
            if(!isset($_SESSION["user"])) 
                return error("Vous n'etes pas connecté");
            
            if(!groupGateway::hasAccess($groupId, $_SESSION["user"]->get_id())) 
                return error("Vous n'avez pas la permission d'autoriser");
        }

        if(! groupGateway::revoke($email, $groupId)) return;


        return success("Suppression autorisation réussie");
    }

    static function visibility(String $groupId, String $visibility) {
        if(groupGateway::isPrivate($groupId)) {
            if(!isset($_SESSION["user"])) 
                return error("Vous n'etes pas connecté");
            
            if(!groupGateway::hasAccess($groupId, $_SESSION["user"]->get_id())) 
                return error("Vous n'avez pas la permission d'autoriser");
        }

        $visible = filter_var($visibility, FILTER_VALIDATE_BOOLEAN);

        groupGateway::visibility($groupId, $visible);

        return success("Changement de visibilité effectué");
    }

    
}