<?php

Class User{
    protected $id;
    protected $nom;
    protected $prenom;
    protected $email;

    function __construct(int $id, string $nom, string $prenom, string $email) {
        $this->id=$id;
        $this->nom=$nom;
        $this->prenom=$prenom;
        $this->email=$email;
    }

    public function get_id(): int {
        return $this->id;
    }

    public function get_nom(): string {
        return $this->nom;
    }

    public function get_prenom(): string {
        return $this->prenom;
    }

    public function get_email(): string {
        return $this->email;
    }
}
