<?php
require_once(__DIR__.'/gateway/taskGateway.php');
require_once(__DIR__.'/gateway/groupGateway.php');
require_once(__DIR__.'/../controller/connection.php');
require_once(__DIR__.'/../controller/message.php');
require_once(__DIR__.'/../controller/session.php');
require_once(__DIR__."/../config/validation.php");

class mdlTask {

    static function getTask(String $idGroup){

        $results = taskGateway::getTask($idGroup);
        echo json_encode($results);
    }

    static function createTask(String $nom, String $group){

        if(! validation::val_createTache($nom, $group)) return;

        if(groupGateway::isPrivate($group)) {
            if(!isset($_SESSION["user"])) 
                return error("Vous n'etes pas connecté");
            
            if(!groupGateway::hasAccess($group, $_SESSION["user"]->get_id())) 
                return error("Vous n'avez pas la permission d'ajouter une tache a ce groupe");
        }
      
        $task = taskGateway::createTask($nom, $group);

        echo json_encode($task);
    }

    static function deleteTask(String $taskId, String $groupId){
        if(groupGateway::isPrivate($groupId)) {
            if(!isset($_SESSION["user"])) 
                return error("Vous n'etes pas connecté");
            
            if(!groupGateway::hasAccess($groupId, $_SESSION["user"]->get_id())) 
                return error("Vous n'avez pas la permission de supprimer une tache de ce groupe");
        }

        taskGateway::deleteTask($taskId);

    }

    static function completeTask(String $groupId, String $taskId, String $etat) {
        if(groupGateway::isPrivate($groupId)) {
            if(!isset($_SESSION["user"])) 
                return error("Vous n'etes pas connecté");
            
            if(!groupGateway::hasAccess($groupId, $_SESSION["user"]->get_id())) 
                return error("Vous n'avez pas la permission de supprimer une tache de ce groupe");
        }

        $etat = filter_var($etat, FILTER_VALIDATE_BOOLEAN);

        taskGateway::completeTask($taskId, $etat);
    }
}