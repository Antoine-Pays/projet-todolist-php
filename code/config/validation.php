<?php

class validation {
    static function val_login(string $email, string $password) {
        self::val_email($email);

        if(! isset($password) || $password == "") return error("Le champ 'Mot de passe' doit être rempli"); 

        return true;
    }

    static function val_register(string $nom, string $prenom, string $email, string $password) {
        if(! isset($nom) || $nom == "") return error("Le champ 'Nom' doit être rempli");
        if($nom != filter_var($nom, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES)) return error("Le nom saisie n'est pas correct");

        if(! isset($prenom) || $prenom == "") return error("Le champ 'Prénom' doit être rempli");
        if($prenom != filter_var($prenom, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES)) return error("Le prénom saisie n'est pas correct");

        self::val_email($email);

        if(! isset($password)) return error("Le champ 'Mot de passe' doit être rempli");

        return true;
    }

    static function val_createGroup(string $nom, string $privee) {
        if(! isset($nom) || $nom == "") return error("Le champ 'Nom' doit être rempli");

        if(filter_var($privee, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL) return error("La checkbox n'est pas valide");

        return true;
    }

    static function val_createTache(string $nom, string $groupe) {
        if(! isset($nom) || $nom == "") return error("La description doit être rempli");

        if(! isset($groupe) || $groupe == "") return error("Groupe non renseigné");
        if($groupe != filter_var($groupe, FILTER_SANITIZE_NUMBER_INT)) return error("Le groupe renseigné n'est pas correct");

        return true;
    }

    static function val_email(string $email) {
        if(! isset($email) || $email == "") return error("Le champ 'Email' n'est pas renseigné");
        if(! filter_var($email, FILTER_VALIDATE_EMAIL)) return error("L'email n'est pas valide");

        return true;
    }

}