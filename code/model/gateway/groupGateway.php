<?php
require_once(__DIR__."/../group.php");
require_once(__DIR__."/../../controller/session.php");
require_once(__DIR__.'/../../controller/connection.php');

class groupGateway {
    static function getGroup() {

        $tab = []; 
        $conn = Connection::getConnection();
        
        $userId = isset($_SESSION["user"]) ? $_SESSION["user"]->get_id() : -1;
        
        $query = "SELECT * FROM groups g WHERE g.private = 0 OR ( (SELECT count(*) FROM users_groups ug WHERE ug.user_id=:id AND ug.group_id = g.id) > 0)";
        $results = $conn->executeQuery(
            $query,
            [
                ':id'=> array($userId, PDO::PARAM_INT)
            ]
        );
        
        if($results == NULL) return NULL;
        
        foreach($results as $groupResult) {
            $tab[] = new Group($groupResult["name"], $groupResult["id"], $groupResult["private"]);
        }
        
        return $tab;
    }
    
    static function createGroup(String $nom, String $privee = ''){
    
        $conn = Connection::getConnection();

        $query = "INSERT INTO groups(name, private) VALUES(:nom, :privee)";
        $conn->executeUpdate(
            $query,
            [
                ':nom'=> array($nom, PDO::PARAM_STR),
                ':privee'=> array($privee, PDO::PARAM_INT)
            ]
        );

        $groupId = $conn->lastInsertId();

        if($privee) {
            $query = "INSERT INTO users_groups VALUES(:user, :group)";
            $conn->executeUpdate(
                $query,
                [
                    ':user'=> array($_SESSION['user']->get_id(), PDO::PARAM_INT),
                    ':group'=> array($groupId, PDO::PARAM_INT)
                ]
            );
        }

        return new Group($nom, $groupId, $privee);
    }

    static function deleteGroup(String $groupId){

        $conn = Connection::getConnection();

        $query = "DELETE FROM groups WHERE id=:groupId";
        $conn->executeUpdate(
            $query,
            [
                ':groupId' => array($groupId, PDO::PARAM_INT)
            ]
        );
    }

    static function authorize(String $email, String $groupId) {
        $conn = Connection::getConnection();
        
        $query = "SELECT id FROM users where email=:email";
        $results1 = $conn->executeQuery(
            $query,
            [
                ':email'=> array($email, PDO::PARAM_STR)
            ]
        );

        if($results1 == NULL) return error("Erreur introuvable dans la BDD fonction autoriser");
        $id = $results1[0]['id'];

        $query = "SELECT * FROM users_groups WHERE user_id=:user AND group_id=:group";
        $results2 = $conn->executeQuery(
            $query,
            [
                ':user'=> array($id, PDO::PARAM_INT),
                ':group'=> array($groupId, PDO::PARAM_INT)
            ]
        );

        if($results2 != NULL) return error("La personne que vous essayez d'ajouter est déja dans le groupe");
        
        $query = "INSERT INTO users_groups VALUES(:user, :group)";
        $conn->executeUpdate(
            $query,
            [
                ':user'=> array($id, PDO::PARAM_INT),
                ':group'=> array($groupId, PDO::PARAM_INT)
            ]
        );

        return true;
    }

    static function revoke(String $email, String $groupId) {
        $conn = Connection::getConnection();
        
        $query = "SELECT id FROM users where email=:email";
        $results1 = $conn->executeQuery(
            $query,
            [
                ':email'=> array($email, PDO::PARAM_STR)
            ]
        );

        if($results1 == NULL) return error("Erreur introuvable dans la BDD fonction revoke");
        $id = $results1[0]['id'];

        $query = "SELECT * FROM users_groups WHERE user_id=:user AND group_id=:group";
        $results2 = $conn->executeQuery(
            $query,
            [
                ':user'=> array($id, PDO::PARAM_INT),
                ':group'=> array($groupId, PDO::PARAM_INT)
            ]
        );

        if($results2 == NULL) return error("La personne que vous essayez de supprimer n'est pas dans le groupe");
        
        $query = "DELETE FROM users_groups WHERE user_id=:user AND group_id=:group";
        $conn->executeUpdate(
            $query,
            [
                ':user'=> array($id, PDO::PARAM_INT),
                ':group'=> array($groupId, PDO::PARAM_INT)
            ]
        );

        return true;
    }

    static function visibility(String $groupId, bool $visibility) {
        $conn = Connection::getConnection();

        $query = "UPDATE groups SET private = :etat WHERE id = :groupId";
        $conn->executeUpdate(
            $query,
            [
                ':groupId' => array($groupId, PDO::PARAM_INT),
                ':etat' => array($visibility, PDO::PARAM_BOOL)
            ]
        );

        $userId = isset($_SESSION["user"]) ? $_SESSION["user"]->get_id() : -1;

        $query = "SELECT * FROM users_groups WHERE user_id=:user AND group_id=:group";
        $results = $conn->executeQuery(
            $query,
            [
                ':user'=> array($userId, PDO::PARAM_INT),
                ':group'=> array($groupId, PDO::PARAM_INT)
            ]
        );

        if($results != NULL) return;

        $query = "INSERT INTO users_groups VALUES(:user, :group)";
        $conn->executeUpdate(
            $query,
            [
                ':user'=> array($userId, PDO::PARAM_INT),
                ':group'=> array($groupId, PDO::PARAM_INT)
            ]
        );

    }

    static function hasAccess(String $groupId, String $userId){
        $conn = Connection::getConnection();
        
        $query = "SELECT count(*) cnt FROM users_groups where group_id=:groupId AND user_id=:userId";
        $results = $conn->executeQuery(
            $query,
            [
                ':groupId'=> array($groupId, PDO::PARAM_INT),
                ':userId'=> array($userId, PDO::PARAM_INT)
            ]
        );

        if($results == NULL) return false;
        return $results[0]['cnt'];
    }

    static function isPrivate(String $groupId) {
        $conn = Connection::getConnection();
        
        $query = "SELECT * FROM groups where id=:groupId";
        $results = $conn->executeQuery(
            $query,
            [
                ':groupId'=> array($groupId, PDO::PARAM_INT)
            ]
        );

        if($results == NULL) return false;
        return filter_var($results[0]['private'], FILTER_VALIDATE_BOOLEAN);
    }
}
