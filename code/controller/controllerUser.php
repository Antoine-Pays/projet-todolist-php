<?php

require_once(__DIR__."/message.php");
require_once(__DIR__.'/../model/mdlUser.php');
require_once(__DIR__.'/../model/mdlGroup.php');
require_once(__DIR__.'/../model/mdlTask.php');

class controllerUser {
    function __construct()
    {
        try {
            $action = $_GET['action'];
        
            switch ($action) {
                case 'NULL':
                    
                break;
                
                case "logout":
                    mdlUser::deconnexion();

                    break;
                              
                case 'group':
                    mdlGroup::getGroup();

                    break;

                case 'createGroup':
                    $nom = $_POST["nom"];
                    $privee = $_POST["privee"];

                    mdlGroup::createGroup($nom, $privee);

                    break;

                case 'deleteGroup':
                    $groupId = $_POST["group"];
    
                    mdlGroup::deleteGroup($groupId);
    
                    break;

                case 'authorizeUser':
                    $email = $_POST["email"];
                    $groupId = $_POST["group"];
                    mdlGroup::authorize($email, $groupId);
            
                    break;

                case 'rejectUser':
                    $email = $_POST["email"];
                    $groupId = $_POST["group"];
                    mdlGroup::revoke($email, $groupId);
            
                    break;
                    
                case 'groupVisibility':
                    $group = $_POST["group"];
                    $visibility = $_POST["visibility"];
                    mdlGroup::visibility($group, $visibility);

                    break;
                case 'task':
                    $groupId = $_POST["group"];
                    mdlTask::getTask($groupId);
        
                    break;

                case 'createTask':
                    $nom = $_POST["nom"];
                    $group = $_POST["group"];
                    
                    mdlTask::createTask($nom, $group);

                    break;

                case 'deleteTask':
                    $groupId = $_POST["group"];
                    $taskId = $_POST["taskId"];
                    mdlTask::deleteTask($taskId, $groupId);
            
                    break;

                case 'completeTask':    
                    $groupId = $_POST["group"];
                    $taskId = $_POST["taskId"];
                    $completed = $_POST["completed"];
                    mdlTask::completeTask($groupId, $taskId, $completed);
        
                    break; 
                    
                default:
                    error("Erreur d'appel controlleur User");
                    break;
            }
                
        } catch (PDOException $e) {
            error("Erreur inattendue BDD controller user" . $e);
        } catch (Exception $e2) {
            error("Erreur inattendue controller user". $e2);
        }
    }
}

    
    